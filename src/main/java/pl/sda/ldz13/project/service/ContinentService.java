package pl.sda.ldz13.project.service;

import pl.sda.ldz13.project.Model.Continent;

import java.util.List;
import java.util.Optional;

public interface ContinentService {

    Continent getContinentById(Long id);

    List<Continent> getAllContinent();

    List<Continent> getContinentByName(String name);

    // ---------------------- metody do zarządzania przez administratora ---------------------------------------

    Continent addContinent(Continent continent);

    Continent modifyContinent(Continent continent);

    boolean deleteContinent(Long id);
}
