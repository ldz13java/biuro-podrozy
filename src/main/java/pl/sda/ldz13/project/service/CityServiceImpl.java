package pl.sda.ldz13.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.ldz13.project.Model.City;
import pl.sda.ldz13.project.repository.AirportJpaRepository;
import pl.sda.ldz13.project.repository.CityJpaRepository;

import java.util.List;

@Service
public class CityServiceImpl implements CityService {

    @Autowired
    private CityJpaRepository cityJpaRepository;

    public List<City> getAllCity() {
        return cityJpaRepository.findAll();
    }

    @Override
    public List<City> getCityByName(String name) {

        return cityJpaRepository.findByNameIgnoreCaseContaining(name);
    }

    @Override
    public List<City> getCityByCountryId(Long countryId) {
        return cityJpaRepository.findByCountry(countryId);
    }

    // ---------------------- metody do zarządzania przez administratora ---------------------------------------

    @Override
    public City addCity(City city) {
        return cityJpaRepository.save(city);
    }

    @Override
    public City modifyCity(City city) {
        return cityJpaRepository.save(city);
    }

    @Override
    public boolean deleteCity(Long id) {
        try{
            cityJpaRepository.deleteById(id);
        }catch (Exception e){
            return false;
        }
        return true;
    }

    @Override
    public City getCityById(Long id) {
        return  cityJpaRepository.findById(id).get();
    }


}
