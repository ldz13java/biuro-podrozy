package pl.sda.ldz13.project.service;

import pl.sda.ldz13.project.Model.Airport;

import java.util.List;

public interface AirportService {

    List<Airport> getAllAirport();

    List<Airport> getAirportByName(String name);

    List<Airport> getAirportByCity(String name);

    List<Airport> getAirportByCityId(Long cityId);

    Airport getById(Long airportId);



    // ---------------------- metody do zarządzania przez administratora ---------------------------------------


    Airport addAirport(Airport airport);

    Airport modifyAirport(Airport airport);

    boolean deleteAirport(Long id);



}
