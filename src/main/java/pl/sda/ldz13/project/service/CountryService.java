package pl.sda.ldz13.project.service;

import pl.sda.ldz13.project.Model.Country;

import java.time.LocalDate;
import java.util.List;

public interface CountryService {


    List<Country> getAllCountry();

    List<Country> getCountryByName(String name);

    List<Country> getCountryByContinentId(Long continentId);


    // ---------------------- metody do zarządzania przez administratora ---------------------------------------

    Country addCountry(Country country);

    Country modifyCountry(Country country);

    boolean deleteCountry(Long id);


    Country getCountryById(Long id);
}
