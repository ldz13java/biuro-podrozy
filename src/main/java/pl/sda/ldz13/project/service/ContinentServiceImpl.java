package pl.sda.ldz13.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.ldz13.project.Model.Continent;
import pl.sda.ldz13.project.repository.ContinentJpaRepository;
import java.util.List;


@Service
public class ContinentServiceImpl implements ContinentService{

    @Autowired
    private ContinentJpaRepository continentJpaRepository;


    @Override
    public Continent getContinentById(Long id) {
        return continentJpaRepository.findById(id).get();
    }

    @Override
    public List<Continent> getAllContinent() {
        return continentJpaRepository.findAll();
    }

    @Override
    public List<Continent> getContinentByName(String name) {

        return continentJpaRepository.findByNameIgnoreCaseContaining(name);
    }




    // ---------------------- metody do zarządzania przez administratora ---------------------------------------
    @Override
    public Continent addContinent(Continent continent) {
        return continentJpaRepository.save(continent);
    }

    @Override
    public Continent modifyContinent(Continent continent) {
        return continentJpaRepository.save(continent);
    }

    @Override
    public boolean deleteContinent(Long id) {
        try{
            continentJpaRepository.deleteById(id);
        }catch (Exception e){
            return false;
        }
        return true;
    }
}
