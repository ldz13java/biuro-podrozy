package pl.sda.ldz13.project.service;

import pl.sda.ldz13.project.Model.City;
import pl.sda.ldz13.project.Model.Enum.Standard;
import pl.sda.ldz13.project.Model.Hotel;

import java.util.List;

public interface HotelService {

    List<Hotel> getAllHotels();

    List<Hotel> getHotelByName(String name);

    List<Hotel> getHotelByStandard(Standard standard);

    List<Hotel> getHotelByCity(Long cityId);

    Hotel getHotelById(Long hotelId);

    Hotel addHotel(Hotel hotel);


}
