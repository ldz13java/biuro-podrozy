package pl.sda.ldz13.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.ldz13.project.Model.PurchasedTrip;
import pl.sda.ldz13.project.repository.PurchasedJpaRepository;

import java.util.List;

@Service
public class PurchasedTripServiceImpl implements PurchasedTripService {

    @Autowired
    private PurchasedJpaRepository purchasedJpaRepository;

    @Override
    public List<PurchasedTrip> getAllPurchasedTrip() {
        return purchasedJpaRepository.findAll();
    }

    @Override
    public List<PurchasedTrip> getPurchasedTryp(String name) {
        return null;
    }
}
