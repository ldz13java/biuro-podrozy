package pl.sda.ldz13.project.service;


import pl.sda.ldz13.project.Model.Airport;
import pl.sda.ldz13.project.Model.City;

import java.util.List;

public interface CityService {

    List<City> getAllCity();

    List<City> getCityByName(String name);

    List<City> getCityByCountryId(Long countryId);



    // ---------------------- metody do zarządzania przez administratora ---------------------------------------


    City addCity(City city);

    City modifyCity(City city);

    boolean deleteCity(Long id);

    City getCityById(Long cityId);
}
