package pl.sda.ldz13.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.ldz13.project.Model.Airport;
import pl.sda.ldz13.project.Model.City;
import pl.sda.ldz13.project.Model.Hotel;
import pl.sda.ldz13.project.Model.Trip;
import pl.sda.ldz13.project.repository.TripJpaRepository;

import java.util.List;

@Service
public class TripServiceImpl implements TripService {

    @Autowired
    private TripJpaRepository tripJpaRepository;


    @Override
    public Trip addTrip(Trip trip) {
        return tripJpaRepository.save(trip);
    }

    @Override
    public List<Trip> getAllTrps() {
        return tripJpaRepository.findAll();
    }

    @Override
    public List<Trip> getTripsByAriportTo(String name) {
        return null;
    }

    @Override
    public List<Trip> getTripsByAriportFrom(Airport airportFrom) {
        return null;
    }

    @Override
    public List<Trip> getTripsByHotel(Hotel hotel) {
        return null;
    }

    @Override
    public List<Trip> getTripsByCity(City city) {
        return null;
    }
}
