package pl.sda.ldz13.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.ldz13.project.Model.Country;
import pl.sda.ldz13.project.repository.CountryJpaRepository;

import java.util.List;
@Service
public class CountryServiceImpl implements CountryService {
    @Autowired
    private CountryJpaRepository countryJpaRepository;


    @Override
    public List<Country> getAllCountry() {
        return countryJpaRepository.findAll();
    }

    @Override
    public List<Country> getCountryByName(String name) {
        return countryJpaRepository.findByNameIgnoreCaseContaining(name);
    }

    @Override
    public List<Country> getCountryByContinentId(Long continentId) {
        return countryJpaRepository.findByContinent(continentId);
    }

    // ---------------------- metody do zarządzania przez administratora ---------------------------------------

    @Override
    public Country addCountry(Country country) {

        return countryJpaRepository.save(country);
    }

    @Override
    public Country modifyCountry(Country country) {
        return countryJpaRepository.save(country);
    }

    @Override
    public boolean deleteCountry(Long id) {
        try {
            countryJpaRepository.deleteById(id);
        }catch (Exception e){
            return false;
        }
        return true;
    }

    @Override
    public Country getCountryById(Long id) {
        return countryJpaRepository.findById(id).get();
    }
}

