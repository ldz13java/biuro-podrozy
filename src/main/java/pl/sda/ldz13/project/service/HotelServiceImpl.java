package pl.sda.ldz13.project.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.ldz13.project.Model.Enum.Standard;
import pl.sda.ldz13.project.Model.Hotel;
import pl.sda.ldz13.project.repository.HotelJpaRepository;

import java.util.List;

@Service
public class HotelServiceImpl implements HotelService{

    @Autowired
    private HotelJpaRepository hotelJpaRepository;

    @Override
    public List<Hotel> getAllHotels() {
        return hotelJpaRepository.findAll();
    }

    @Override
    public List<Hotel> getHotelByName(String name) {
        return null;
    }

    @Override
    public List<Hotel> getHotelByStandard(Standard standard) {
        return null;
    }

    @Override
    public List<Hotel> getHotelByCity(Long cityId) {
        return hotelJpaRepository.findByCity(cityId);
    }

    @Override
    public Hotel getHotelById(Long hotelId) {
        return hotelJpaRepository.findById(hotelId).get();
    }

    @Override
    public Hotel addHotel(Hotel hotel) {
        return hotelJpaRepository.save(hotel);
    }

}
