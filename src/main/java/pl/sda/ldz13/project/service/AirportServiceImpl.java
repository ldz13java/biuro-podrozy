package pl.sda.ldz13.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.ldz13.project.Model.Airport;
import pl.sda.ldz13.project.repository.AirportJpaRepository;

import java.util.List;

@Service
public class AirportServiceImpl implements AirportService {

    @Autowired
    private AirportJpaRepository airportJpaRepository;

    @Override
    public List<Airport> getAllAirport() {
        return airportJpaRepository.findAll();
    }

    @Override
    public List<Airport> getAirportByName(String name) {
        return airportJpaRepository.findByNameIgnoreCaseContaining(name);
    }

    @Override
    public List<Airport> getAirportByCity(String name) {
        return null;
    }

    @Override
    public List<Airport> getAirportByCityId(Long cityId) {

        return airportJpaRepository.findByCityId(cityId);
    }

    @Override
    public Airport getById(Long airportId) {
        return airportJpaRepository.findById(airportId).get();
    }


    // ---------------------- metody do zarządzania przez administratora ---------------------------------------
    @Override
    public Airport addAirport(Airport airport) {
        return airportJpaRepository.save(airport);
    }

    @Override
    public Airport modifyAirport(Airport airport) {
        return airportJpaRepository.save(airport);
    }

    @Override
    public boolean deleteAirport(Long id) {
        try{
            airportJpaRepository.deleteById(id);
        }catch (Exception e){
            return false;
        }
        return true;
    }
}
