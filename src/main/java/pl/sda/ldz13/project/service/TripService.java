package pl.sda.ldz13.project.service;

import pl.sda.ldz13.project.Model.Airport;
import pl.sda.ldz13.project.Model.City;
import pl.sda.ldz13.project.Model.Hotel;
import pl.sda.ldz13.project.Model.Trip;

import java.util.List;

public interface TripService {

    Trip addTrip(Trip trip);

    List<Trip> getAllTrps();

    List<Trip> getTripsByAriportTo(String name);

    List<Trip> getTripsByAriportFrom(Airport airportFrom);

    List<Trip> getTripsByHotel(Hotel hotel);

    List<Trip> getTripsByCity(City city);
}
