package pl.sda.ldz13.project.service;

import pl.sda.ldz13.project.Model.PurchasedTrip;

import java.util.List;

public interface PurchasedTripService {

    List<PurchasedTrip> getAllPurchasedTrip();

    List<PurchasedTrip> getPurchasedTryp(String name);


}
