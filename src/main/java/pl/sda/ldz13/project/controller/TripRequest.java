package pl.sda.ldz13.project.controller;

// klasa tłumaczaca pola z frontu
public class TripRequest {

    Long id;
    private String tripName;
    private Long contId; // klucz obcy
    private Long countryId;
    private Long cityId;
    private Long hotelToId;
    private Long countOfPeople;
    private String description;
    private String picture;
    private Long airportTo;
    private Long airportFrom;

    public Long getAirportTo() {
        return airportTo;
    }

    public void setAirportTo(Long airportTo) {
        this.airportTo = airportTo;
    }

    public Long getAirportFrom() {
        return airportFrom;
    }

    public void setAirportFrom(Long airportFrom) {
        this.airportFrom = airportFrom;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTripName() {
        return tripName;
    }

    public void setTripName(String tripName) {
        this.tripName = tripName;
    }

    public Long getContId() {
        return contId;
    }

    public void setContId(Long contId) {
        this.contId = contId;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Long getHotelToId() {
        return hotelToId;
    }

    public void setHotelToId(Long hotelToId) {
        this.hotelToId = hotelToId;
    }

    public Long getCountOfPeople() {
        return countOfPeople;
    }

    public void setCountOfPeople(Long countOfPeople) {
        this.countOfPeople = countOfPeople;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
