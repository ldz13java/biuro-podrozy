package pl.sda.ldz13.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sda.ldz13.project.Model.City;
import pl.sda.ldz13.project.service.CityService;
import pl.sda.ldz13.project.service.CountryService;

import java.util.List;

@RestController
@RequestMapping("rest/cities")
public class CityRestController {

    @Autowired
    private final CityService cityService;

    @Autowired
    private final CountryService countryService;


    public CityRestController(CityService cityService, CountryService countryService) {
        this.cityService = cityService;
        this.countryService = countryService;
    }


    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.POST, value = "/save")
    public ResponseEntity<City> save(@RequestBody CityApi cityApi) {

        City cityEntity = new City();

        cityEntity.setId(cityApi.getId());
        cityEntity.setName(cityApi.getName());
        cityEntity.setCountry(countryService.getCountryById(cityApi.countryId));

        City cityUser = cityService.addCity(cityEntity);
        return ResponseEntity.ok(cityUser);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(value = "/all")
    public ResponseEntity<List<City>> getAll() {
        return ResponseEntity.ok(cityService.getAllCity());
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(value = "/citiesByCountry" )
    public ResponseEntity<List<City>> getCityByCountryId(@RequestParam Long countryId) {
        return ResponseEntity.ok(cityService.getCityByCountryId(countryId));
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(value = "/{id}")
    private ResponseEntity<CityApi> getCity(@PathVariable("id") Long id){

        City cityById = cityService.getCityById(id);
        CityApi cityApi = new CityApi();

        cityApi.setId(cityById.getId());
        cityApi.setName(cityById.getName());
        cityApi.setCountryId(cityById.getCountry().getId());

        return ResponseEntity.ok(cityApi);

    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.POST, value = "/modify")
    private ResponseEntity<City> modify(@RequestBody CityApi cityApi){

        City cityEntity = new City();

        cityEntity.setId(cityApi.getId());
        cityEntity.setName(cityApi.getName());
        cityEntity.setCountry(countryService.getCountryById(cityApi.countryId));
        City cityUser = cityService.modifyCity(cityEntity);

        return ResponseEntity.ok(cityUser);
    }


}
