package pl.sda.ldz13.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;
import pl.sda.ldz13.project.Model.Airport;
import pl.sda.ldz13.project.service.AirportService;
import pl.sda.ldz13.project.service.CityService;

import java.util.List;

@Repository
@RequestMapping("rest/airports")
public class AirportRestController {

    @Autowired
    private final AirportService airportService;

    @Autowired
    private final CityService cityService;

    public AirportRestController(AirportService airportService, CityService cityService) {
        this.airportService = airportService;
        this.cityService = cityService;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.POST, value = "/save")
    private ResponseEntity<Airport> save(@RequestBody AirportRequest airportRequest) {

        Airport airportEntity = new Airport();

        airportEntity.setId(airportRequest.getId());
        airportEntity.setName(airportRequest.getName());
        airportEntity.setCity(cityService.getCityById(airportRequest.getCityId()));

        Airport airportUser = airportService.addAirport(airportEntity);
        return ResponseEntity.ok(airportUser);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/all")
    private ResponseEntity<List<Airport>> getAll(){
        return ResponseEntity.ok(airportService.getAllAirport());
    }


    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/airportByCity")
    private ResponseEntity<List<Airport>> getAirportByCityId(@RequestParam Long cityId){
        return ResponseEntity.ok(airportService.getAirportByCityId(cityId));
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/")
    private ResponseEntity<Airport> getAirport(@RequestParam Long id){
        return ResponseEntity.ok(airportService.getById(id));
    }
}
