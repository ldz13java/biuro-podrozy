package pl.sda.ldz13.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sda.ldz13.project.Model.City;
import pl.sda.ldz13.project.Model.Country;
import pl.sda.ldz13.project.service.ContinentService;
import pl.sda.ldz13.project.service.CountryService;

import java.util.List;

@RestController
@RequestMapping("rest/countries")
public class CountryRestController {

    @Autowired
    private final CountryService countryService;
    @Autowired
    private ContinentService continentService;

    public CountryRestController(CountryService countryService) {
        this.countryService = countryService;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.POST, value = "/save")
    public ResponseEntity<Country> save(@RequestBody CountryRequest countryRequest) {
        Country countryEntity = new Country();
        countryEntity.setId(countryRequest.getId());
        countryEntity.setName(countryRequest.getName());
        countryEntity.setContinent(continentService.getContinentById(countryRequest.contId));

        Country countryUser = countryService.addCountry(countryEntity);
        // kod 200 zwraca metoda .ok()
        return ResponseEntity.ok(countryUser);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(value = "/all")
    public ResponseEntity<List<Country>> getAll() {
        return ResponseEntity.ok(countryService.getAllCountry());
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(value = "/countriesByContinents")
    public ResponseEntity<List<Country>> getcountryByContinentId(@RequestParam Long continentId) {
        return ResponseEntity.ok(countryService.getCountryByContinentId(continentId));
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(value = "/countryById")
    public ResponseEntity<Country> getCountryId(@RequestParam Long countryId) {
        return ResponseEntity.ok(countryService.getCountryById(countryId));
    }


}
