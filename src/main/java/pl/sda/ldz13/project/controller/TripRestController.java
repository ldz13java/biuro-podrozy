package pl.sda.ldz13.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.sda.ldz13.project.Model.Trip;
import pl.sda.ldz13.project.service.*;

@RestController
@RequestMapping("rest/trips")
public class TripRestController {

    @Autowired
    private final TripService tripService;

    @Autowired
    private ContinentService continentService;

    @Autowired
    private CountryService countryService;

    @Autowired
    private CityService cityService;

    @Autowired
    private HotelService hotelService;

    @Autowired
    private AirportService airportService;


    public TripRestController(TripService tripService) {
        this.tripService = tripService;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/save")
    public ResponseEntity<Trip> save(@RequestBody TripRequest tripRequest){
        Trip tripEntity = new Trip();

        tripEntity.setId(tripRequest.getId());
        tripEntity.setContinent(continentService.getContinentById(tripRequest.getContId()));
        tripEntity.setCountry(countryService.getCountryById(tripRequest.getCountryId()));
        tripEntity.setCityTo(cityService.getCityById(tripRequest.getCityId()));
        tripEntity.setHotelTo(hotelService.getHotelById(tripRequest.getHotelToId()));
        tripEntity.setCountOfPerson(tripRequest.getCountOfPeople());
        tripEntity.setDescription(tripRequest.getDescription());
        tripEntity.setAiportTo(airportService.getById(tripRequest.getAirportTo()));
        tripEntity.setAiportFrom(airportService.getById(tripRequest.getAirportTo()));

  
        Trip tripUser = tripService.addTrip(tripEntity);
        return ResponseEntity.ok(tripUser);

    }
}
