package pl.sda.ldz13.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;
import pl.sda.ldz13.project.Model.Enum.Standard;
import pl.sda.ldz13.project.Model.Hotel;
import pl.sda.ldz13.project.service.CityService;
import pl.sda.ldz13.project.service.HotelService;

import java.util.List;

@Repository
@RequestMapping("rest/hotels")
public class HotelRestController {

    @Autowired
    private final HotelService hotelService;

    @Autowired
    private final CityService cityService;


    public HotelRestController(HotelService hotelService, CityService cityService) {
        this.hotelService = hotelService;
        this.cityService = cityService;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.POST, value = "/save")
    public ResponseEntity<Hotel> save(@RequestBody HotelRequest hotelRequest) {

        Hotel hotelEntity = new Hotel();

        hotelEntity.setId(hotelRequest.getId());
        hotelEntity.setName(hotelRequest.getName());
        hotelEntity.setDescription(hotelRequest.getDescription());
        hotelEntity.setCity(cityService.getCityById(hotelRequest.getCityId()));
        hotelEntity.setStandard(Standard.valueOf(hotelRequest.getStandard()));

        Hotel hotelUser = hotelService.addHotel(hotelEntity);
        return ResponseEntity.ok(hotelUser);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(value = "/all")
    public ResponseEntity<List<Hotel>> getAll() {
        return ResponseEntity.ok(hotelService.getAllHotels());
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(value = "/hotelsByCity")
    public ResponseEntity<List<Hotel>> getHotelsByCity(@RequestParam Long cityId) {
        return ResponseEntity.ok(hotelService.getHotelByCity(cityId));
    }

}
