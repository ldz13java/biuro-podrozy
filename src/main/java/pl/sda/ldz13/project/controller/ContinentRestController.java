package pl.sda.ldz13.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sda.ldz13.project.Model.Continent;
import pl.sda.ldz13.project.service.ContinentService;

import java.util.List;

@RestController
@RequestMapping("rest/continents")
public class ContinentRestController {

    @Autowired
    private final ContinentService continentService;

    public ContinentRestController(ContinentService continentService) {
        this.continentService = continentService;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.POST, value = "/save")
    public ResponseEntity<Continent> save(@RequestBody Continent continent) {
        Continent continentUser = continentService.addContinent(continent);
        // kod 200 zwraca metoda .ok()
        return ResponseEntity.ok(continentUser);
    }
    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(value = "/all")
    public ResponseEntity<List<Continent>> getAll() {
        return ResponseEntity.ok(continentService.getAllContinent());
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(value = "/{id}")
    private ResponseEntity<Continent> getContinent(@PathVariable("id") Long id){
        return ResponseEntity.ok(continentService.getContinentById(id));
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.PUT, value = "/modify")
    private ResponseEntity<Continent> modify(@RequestBody Continent continent){
        Continent continentUser = continentService.modifyContinent(continent);
        return ResponseEntity.ok(continentUser);
    }
    
}
