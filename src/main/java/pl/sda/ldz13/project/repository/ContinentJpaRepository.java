package pl.sda.ldz13.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.ldz13.project.Model.Continent;
import java.util.List;



@Repository
public interface ContinentJpaRepository extends JpaRepository<Continent, Long> {
    List<Continent> findByNameIgnoreCaseContaining(String name);


}
