package pl.sda.ldz13.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.ldz13.project.Model.Trip;

@Repository
public interface TripJpaRepository extends JpaRepository<Trip, Long> {
}
