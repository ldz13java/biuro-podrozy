package pl.sda.ldz13.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.sda.ldz13.project.Model.Airport;

import java.util.List;

@Repository
public interface AirportJpaRepository extends JpaRepository<Airport, Long> {

    List<Airport> findByNameIgnoreCaseContaining(String name);


    @Query("select airport from pl.sda.ldz13.project.Model.Airport airport inner join "
            + "airport.city city where city.id = :cityId")
    List<Airport> findByCityId(@Param(value = "cityId") Long cityId);


}
