package pl.sda.ldz13.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.sda.ldz13.project.Model.Hotel;

import java.util.List;

@Repository
public interface HotelJpaRepository extends JpaRepository<Hotel, Long> {

    @Query("select hotel from pl.sda.ldz13.project.Model.Hotel hotel inner join "
            + "hotel.city city where city.id = :cityId")
    List<Hotel> findByCity(@Param(value = "cityId") Long cityId);
}
