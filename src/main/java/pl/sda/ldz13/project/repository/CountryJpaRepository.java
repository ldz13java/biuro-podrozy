package pl.sda.ldz13.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.sda.ldz13.project.Model.City;
import pl.sda.ldz13.project.Model.Country;
import pl.sda.ldz13.project.Model.Hotel;

import java.util.List;

@Repository
public interface CountryJpaRepository extends JpaRepository<Country, Long> {

    List<Country> findByNameIgnoreCaseContaining(String name);

    @Query("select country from pl.sda.ldz13.project.Model.Country country inner join "
            + "country.continent continent where continent.id = :continentId")
    List<Country> findByContinent(@Param(value = "continentId") Long continentId);

}
