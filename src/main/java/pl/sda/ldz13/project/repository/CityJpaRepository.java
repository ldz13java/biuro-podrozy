package pl.sda.ldz13.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.sda.ldz13.project.Model.City;

import java.util.List;

@Repository
public interface CityJpaRepository extends JpaRepository<City, Long> {
    List<City> findByNameIgnoreCaseContaining(String name);

    @Query("select city from pl.sda.ldz13.project.Model.City city inner join "
            + "city.country country where country.id = :countryId")
    List<City> findByCountry(@Param(value = "countryId") Long countryId);
}
