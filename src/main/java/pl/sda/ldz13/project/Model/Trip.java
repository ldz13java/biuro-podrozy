package pl.sda.ldz13.project.Model;

import lombok.Getter;
import lombok.Setter;
import pl.sda.ldz13.project.Model.Enum.Type;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "trips")
@Getter
@Setter
public class Trip {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "airport_from_id")
    private Airport aiportFrom;

    @ManyToOne
    @JoinColumn(name = "airport_to_id")
    private Airport aiportTo;
    @ManyToOne
    @JoinColumn(name = "hotel_id")
    private Hotel hotelTo;
    @ManyToOne
    @JoinColumn(name = "city_id")
    private City cityTo;
    @ManyToOne
    @JoinColumn(name = "continent_id")
    private Continent continent;
    @ManyToOne
    @JoinColumn(name = "country_id")
    private Country country;
    private LocalDate departureDate;
    private LocalDate returnDate;
    private int countOfDate;
    @Enumerated(EnumType.STRING)
    private Type typ;
    private double priceForAdult;
    private double priceForChild;
    private double promotion;
    private double countOfPerson;
    private String description;
    private String picture;

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Continent getContinent() {
        return continent;
    }

    public void setContinent(Continent continent) {
        this.continent = continent;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Airport getAiportFrom() {
        return aiportFrom;
    }

    public void setAiportFrom(Airport aiportFrom) {
        this.aiportFrom = aiportFrom;
    }

    public Airport getAiportTo() {
        return aiportTo;
    }

    public void setAiportTo(Airport aiportTo) {
        this.aiportTo = aiportTo;
    }

    public Hotel getHotelTo() {
        return hotelTo;
    }

    public void setHotelTo(Hotel hotelTo) {
        this.hotelTo = hotelTo;
    }

    public City getCityTo() {
        return cityTo;
    }

    public void setCityTo(City cityTo) {
        this.cityTo = cityTo;
    }

    public LocalDate getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(LocalDate departureDate) {
        this.departureDate = departureDate;
    }

    public LocalDate getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(LocalDate returnDate) {
        this.returnDate = returnDate;
    }

    public int getCountOfDate() {
        return countOfDate;
    }

    public void setCountOfDate(int countOfDate) {
        this.countOfDate = countOfDate;
    }

    public Type getTyp() {
        return typ;
    }

    public void setTyp(Type typ) {
        this.typ = typ;
    }

    public double getPriceForAdult() {
        return priceForAdult;
    }

    public void setPriceForAdult(double priceForAdult) {
        this.priceForAdult = priceForAdult;
    }

    public double getPriceForChild() {
        return priceForChild;
    }

    public void setPriceForChild(double priceForChild) {
        this.priceForChild = priceForChild;
    }

    public double getPromotion() {
        return promotion;
    }

    public void setPromotion(double promotion) {
        this.promotion = promotion;
    }

    public double getCountOfPerson() {
        return countOfPerson;
    }

    public void setCountOfPerson(double countOfPerson) {
        this.countOfPerson = countOfPerson;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
